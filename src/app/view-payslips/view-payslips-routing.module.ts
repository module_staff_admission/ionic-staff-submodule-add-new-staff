import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewPayslipsPage } from './view-payslips.page';

const routes: Routes = [
  {
    path: '',
    component: ViewPayslipsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewPayslipsPageRoutingModule {}
