import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewPayslipsPageRoutingModule } from './view-payslips-routing.module';

import { ViewPayslipsPage } from './view-payslips.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewPayslipsPageRoutingModule
  ],
  declarations: [ViewPayslipsPage]
})
export class ViewPayslipsPageModule {}
