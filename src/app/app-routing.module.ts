import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'view-staff',
    loadChildren: () => import('./view-staff/view-staff.module').then( m => m.ViewStaffPageModule)
  },
  {
    path: 'add-staff',
    loadChildren: () => import('./add-staff/add-staff.module').then( m => m.AddStaffPageModule)
  },
  {
    path: 'update-staff',
    loadChildren: () => import('./update-staff/update-staff.module').then( m => m.UpdateStaffPageModule)
  },
  {
    path: 'view-contract',
    loadChildren: () => import('./view-contract/view-contract.module').then( m => m.ViewContractPageModule)
  },
  {
    path: 'update-contract',
    loadChildren: () => import('./update-contract/update-contract.module').then( m => m.UpdateContractPageModule)
  },
  {
    path: 'add-contract',
    loadChildren: () => import('./add-contract/add-contract.module').then( m => m.AddContractPageModule)
  },
  {
    path: 'new-payslips',
    loadChildren: () => import('./new-payslips/new-payslips.module').then( m => m.NewPayslipsPageModule)
  },
  {
    path: 'view-payslips',
    loadChildren: () => import('./view-payslips/view-payslips.module').then( m => m.ViewPayslipsPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
