import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { StaffService } from '../staff.service';
import { NgForm } from '@angular/forms';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-update-staff',
  templateUrl: './update-staff.page.html',
  styleUrls: ['./update-staff.page.scss'],
})
export class UpdateStaffPage implements OnInit {
  staff_id: any;
  staff: Object;
  selectInputOptions: Object;
  selectedInput: Object;

  constructor(
    private route: ActivatedRoute, 
    private router: Router,
    private staffService: StaffService,
    private navCtrl: NavController
  ) {
    this.staff = {};
    this.selectInputOptions = {};
    this.selectedInput = {};
    this.route.queryParams.subscribe(async params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.staff_id = this.router.getCurrentNavigation().extras.state.staff_id;

        this.staffService.getSelectedStaff(this.staff_id).then((staff) => {
          this.staff = staff;
        });
      }
    });
  }
  
  ngOnInit() {
    this.staffService.getSelectInputOptions().then((res) => {
      this.selectInputOptions = res;
    })
  }

  ionViewWillEnter() {
    this.ngOnInit();
  }

  updateStaff(form: NgForm) {
    for (const [key, value] of Object.entries(form.value)) {
      if (value === '')
        return;

      this.staff[key] = value;
    }
    this.save();
  }

  save(): void {
    if (this.staff) {
      this.staffService.updateStaff(this.staff).then((data) => {
      })
      this.navCtrl.navigateForward('/view-staff');
    }
  }
}
