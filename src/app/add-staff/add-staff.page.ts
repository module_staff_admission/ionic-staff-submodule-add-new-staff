import { Component, OnInit } from '@angular/core';
import { StaffService } from '../staff.service';
import { Observable } from 'rxjs';
import { NgForm } from '@angular/forms';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-add-staff',
  templateUrl: './add-staff.page.html',
  styleUrls: ['./add-staff.page.scss'],
})
export class AddStaffPage implements OnInit {
  selectInputOptions: Object;
  newStaffData: Object;

  constructor(
    private staffService: StaffService,
    private navCtrl: NavController
  ) {
    this.selectInputOptions = {};
    this.newStaffData = {};
  }

  ngOnInit() {
      this.staffService.getSelectInputOptions().then((res) => {
        this.selectInputOptions = res;
      }
    )
  }

  submitStaff(form: NgForm) {
    for (const [key, value] of Object.entries(form.value)) {
      if (value === '')
        return;

      this.newStaffData[key] = value;
    }
    this.save();
  }

  save(): void {
    if (this.newStaffData) {
      this.staffService.createStaff(this.newStaffData).then((data) => {
      })
      this.navCtrl.navigateForward('/view-staff');
    }
  }
}
