import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewPayslipsPage } from './new-payslips.page';

const routes: Routes = [
  {
    path: '',
    component: NewPayslipsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewPayslipsPageRoutingModule {}
